<?php
  
  namespace Website\Controller;
  
  use Website\App\Controller;
  use Website\Model\Manager\Manager;
  
class HomeController extends Controller
{
    public function home()
    {
        //recuperation du manager
        $manager = new Manager();
      
        // recuperation du bonjour
        $donnees = $manager->getBonjour();
        $bonjour = $donnees['contenu'];
        $bonjour_l = $donnees['langue'];

        //on definit la vue et on retourne le resulat
        $this->renderView(
            'accueil', [
            'bonjour' => $bonjour,
            'bonjour_l' => $bonjour_l,
            ]
        );
    }
}
