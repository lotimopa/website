<header>
  <div class="top-bar">
    <div class="top-bar-left">
      <nav role="navigation">
        <ul class="dropdown menu vertical medium-horizontal align-center" data-dropdown-menu>
          <li>
            <a href="/">Accueil</a>
          </li>
          <li>
            <a href="page-parcours.html">Parcours</a>
          </li>
          <li>
            <a href="page-contact.html">Liens et Contact</a>
          </li>
        </ul>
      </nav>
    </div>
  </div>
</header>
